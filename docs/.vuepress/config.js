module.exports = {
  title: 'GitLab + Cypress = ❤️',
  description: 'Static site deployed to GitLab pages and tested using Cypress',
  base: '/gitlab-pages-example/',
  dest: 'public'
}
